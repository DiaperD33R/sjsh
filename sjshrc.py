from os import *

# Variables
name = "diaperd33r"
pro = "@sjsh $ "
autols = 0

if name == "":
	print("Set a name in sjshrc.py")
	quit()
# Functions
def getdir():
	dir = getcwd()
	return dir

# Classes
class Commands:
	def list():
		system("ls")
	def chdir():
		new = input()

		if autols == 1:
			list()

		chdir(new)
	def help():
		print("Super J shell, Mercury, 1.00")
		print("exit: exit sjsh")
		print("ls: lists files in current directory")
		print("cd: changes your current directory")
		print("help: this page")
	def open():
		type = input("Program or script? (p/s) ")
		
		item = input()

		if type.lower() == "p":
			system(item)
		elif type.lower() == "s":
			system("./" + item)
		else:
			print("sjsh: type_invald: \"" + item + "\", not a valid type")
		
	